﻿using System;
using Timer.Interfaces;
using System.Threading;

namespace Timer.Implementation
{
	public class CountDownNotifier : ICountDownNotifier
	{
		protected Timer timer;

		protected string timName;

		protected int timStat;
		public CountDownNotifier (Timer timer)
		{

			if (timer != null) { this.timer = timer; }
			else { throw new ArgumentNullException(); }

            timer.Started += OnStartedTimHandler;
            timer.Stopped += OnStoppedTimHandler;
            timer.Tick += OnTicksTimHandler;

        }
        public void Init(Action<string, int> startDelegate, Action<string> stopDelegate, Action<string, int> tickDelegate)
		{
			timer.Started += startDelegate;
			timer.Stopped += stopDelegate;
			timer.Tick += tickDelegate;
			//throw new NotImplementedException();
		}

		public void Run()
		{
			int totalTick = timer.NumberOfTicks;

			for (int i = 0; i <= totalTick-1; i++)
			{
				timer.CountDown();
				Thread.Sleep(50);
			}
			//throw new NotImplementedException();
		}

        public void OnTicksTimHandler(string name, int ticks)
        {

            timName = timer.NameOfTimer;
            timStat = timer.NumberOfTicks;

        }

        public void OnStartedTimHandler(string name, int ticks)
        {
            timName = timer.NameOfTimer;
            timStat = timer.NumberOfTicks;
        }

        public void OnStoppedTimHandler(string name)
        {
            timName = timer.NameOfTimer;
            timer.Started -= OnStartedTimHandler;
            timer.Stopped -= OnStoppedTimHandler;
            timer.Tick -= OnTicksTimHandler;
        }


    }
}