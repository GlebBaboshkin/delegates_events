﻿using System;

namespace Timer
{

	public class Timer
	{
		protected string nameOfTimer;
		protected int numberOfTicks;

		internal int chekPoint = 0;
		public Timer(string name, int ticks)
		{
			if (name != "" && name != null && ticks > 0)
			{
				nameOfTimer = name;
				numberOfTicks = ticks;
			}
			else { throw new ArgumentException(); }

		}

		public string NameOfTimer
		{
			get { return nameOfTimer; }
		}

		public int NumberOfTicks
		{
			get { return numberOfTicks; }
		}

		public void CountDown()
		{
			if (chekPoint == 0 && numberOfTicks!=0)
			{ Started.Invoke(NameOfTimer, NumberOfTicks); }

			numberOfTicks -= 1;
			chekPoint += 1;
			if (chekPoint >= 1 && numberOfTicks!=0)
			{ Tick.Invoke(NameOfTimer, NumberOfTicks); }
			
			
			if (numberOfTicks == 0)
			{ chekPoint = 0; Stopped.Invoke(NameOfTimer); }
		}


		public event Action<string, int> Started = delegate { };

		public event Action<string, int> Tick = delegate { };

		public event Action<string> Stopped = delegate { };
	}
}