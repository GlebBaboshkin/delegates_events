﻿using System;
using Timer.Interfaces;
using Timer.Implementation;


namespace Timer.Factories
{
	public class CountDownNotifierFactory
	{
		public ICountDownNotifier CreateNotifierForTimer(Timer timer)
		{
			try
			{
				return new CountDownNotifier(timer);
			}
			catch { throw; }
		}
	}
}
